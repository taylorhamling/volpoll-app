export const environment = {
    production: true,
    apiUrl: 'https://volpoll.org.au/api/public/api',
    reportUrl: 'https://volpoll.org.au/api/public',  
};

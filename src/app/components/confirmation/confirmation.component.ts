import { Component, Inject, ViewEncapsulation  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmationComponent{
    
    public title:string;
    public content:string;
    
    constructor(public dialogRef: MatDialogRef<ConfirmationComponent>,@Inject(MAT_DIALOG_DATA) public data: any){
        this.title = data.title ? data.title : "Confirm";
        this.content = data.content ? data.content : "Are you sure?";
    }
 
    public confirm(){
        this.dialogRef.close({confirm:true});
    }    
    
    public dismiss(){ 
        this.dialogRef.close();
    }            
         
                
    ngOnInit(){}    
        

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {QuestionService} from '../../services/question/question.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminService} from '../../services/admin/admin.service';

@Component({
    selector: 'app-start',
    templateUrl: './start.component.html',
    styleUrls: ['./start.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class StartComponent implements OnInit {
    
    public user:any;
    public errors:Array<any>;
    public loading:boolean;
    public fieldsEnabled:Array<string>;
    
    constructor(private authenticationService: AuthenticationService, private questionService:QuestionService, 
        private route:ActivatedRoute, private router: Router, private adminService:AdminService) { 
    
        this.user = {role_id:2, group_type:"group"};
        this.errors = [];
        
        this.fieldsEnabled = ["first_name","last_name","email","group_type","group","postcode","position","is_peak"];
        
        this.adminService.getSettings().toPromise().then((data) => {
          
            for (let setting of data){
                if (setting.key === "start_fields"){
                    this.fieldsEnabled = JSON.parse(setting.data);
                }                
                
                
                
            }
            
        })        
        
        
        
    }

    ngOnInit() {
        
        this.route.params.subscribe(params =>{

        });         
        
        
    }
    
    
    public start(form){
        if (!form.first_name || !form.last_name || !form.group || !form.postcode || !form.email){
            let message = "Not all required fields are filled in.";
            this.errors = [message];            
            return;
        }
        
        this.loading = true;
        this.errors = [];
        this.authenticationService.register(this.user).toPromise().then((data) => {
            this.loading = false;
            
            //go to first question
            this.questionService.getQuestionsByRole(this.user.role_id).toPromise().then(() => {});
            
            this.router.navigate(['/question/1']); 
            
        }).catch((e) => {
            
            if (e && e.error && e.error.error && e.error.error.errors){
                for (var index in e.error.error.errors){
                    this.errors = this.errors.concat(e.error.error.errors[index]);
                }                 
            }
            else if (e && e.error && e.error.error && e.error.error.message){
                let message = e.error.error.message;
                this.errors = [message];
            }
            else{
                let message = "There was an error.";
                this.errors = [message];
            }
            
            this.loading = false;
        })
    }
    
    public changeRole(roleId){
        this.user.role_id = roleId;
    }

}

import { Component, OnInit } from '@angular/core';
import {ReportService} from '../../services/report/report.service';
import {environment} from '../../../environments/environment';
import { ShareComponent } from './share/share.component';
import {MatDialog} from '@angular/material';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    selector: 'app-end',
    templateUrl: './end.component.html',
    styleUrls: ['./end.component.scss']
})
export class EndComponent implements OnInit {
    
    public report:any;
    public loading:boolean;
    public reportHtml:string;
    
    constructor(private reportService:ReportService, public dialog: MatDialog, private sanitizer:DomSanitizer) { 
        
        this.report = {};
        this.loading = true;
        
        this.reportService.getReportHtml().toPromise().then((data) => {
            console.log(data);
            this.report = data.report;
            
            if (data.file){
                this.report.download = environment.reportUrl + "/" + data.file;
                this.loading = false;  
            }
            else{
                this.reportHtml = atob(data.html);
                
                console.log(this.reportHtml);
                
                
                setTimeout(() => {
                    
                    let iframe:any = document.getElementById("report-html");
        
                    var scripts = iframe.contentWindow.document.documentElement.getElementsByTagName('script');
                    var i = scripts.length;
                    while (i--) {
                      scripts[i].parentNode.removeChild(scripts[i]);
                    }
                    
                                   
                    
                          
           
                    
                                        
                    var src = iframe.contentWindow.document.documentElement.outerHTML;
                    
                    
                  console.log(src);
                    
                    this.reportService.generateReportHtml(btoa(unescape(encodeURIComponent( src )))).toPromise().then((generatedReport) => {
                        this.report.download = environment.reportUrl + "/" + generatedReport.file;
                        this.loading = false;  
                        iframe.remove();
                    });
                    
                    
                },5000);
                            
            }
            
        })
        
        /*
        this.reportService.getReport().toPromise().then((data) => {

            this.report = data;

        })
        */
    
    
    }

    ngOnInit() {
    }
    
    private encodeHtml(html){
        var encodedStr = html.replace(/[\u00A0-\u9999<>\&]/g, function(i) {
           return '&#'+i.charCodeAt(0)+';';
        });  
        return encodedStr;           
    }
    
    private decodeHtml(html){
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;        
    }
    
    public sanitizeHtml(html):SafeHtml{
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }

    
    public share(){
        let dialogRef = this.dialog.open(ShareComponent, {
            width: '400px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(data => {
        })        
    }


}

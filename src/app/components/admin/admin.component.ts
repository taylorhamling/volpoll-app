import { Component, OnInit,ViewEncapsulation,ViewChild,ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog} from '@angular/material';
import {AuthenticationService} from '../../services/authentication/authentication.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
    

    
    constructor(private authenticationService:AuthenticationService, private router: Router) { 
 
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        
        
        
        this.checkAuth();        
        
    
    
    }

    ngOnInit() {
    }
    
    
    private checkAuth(){
        this.authenticationService.isAdminAuthorized().toPromise().then((data) => {
            //this.router.navigate(['/admin/questions']);

            if (!data && 
                (this.router.url === "/admin" 
                || this.router.url === "/admin/questions" 
                || this.router.url === "/admin/stats"
                || this.router.url === "/admin/submissions"
                || this.router.url === "/admin/users"
                || this.router.url === "/admin/settings")){
                this.router.navigate(['/admin/login']);
            }
            else if (data && (this.router.url === "/admin" || this.router.url === "/admin/login")){
                this.router.navigate(['/admin/questions']);
            }
            
        }).catch(() => {
            if (this.router.url === "/admin/questions" 
                || this.router.url === "/admin/stats"
                || this.router.url === "/admin/submissions"
                || this.router.url === "/admin/users"
                || this.router.url === "/admin/settings"){
                this.router.navigate(['/admin/login']);
            }
        });        
    }
    
    
    public logout(){
        this.authenticationService.logout();
    }

}

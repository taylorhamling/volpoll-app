import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthenticationService} from '../../../services/authentication/authentication.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    
    public errors:Array<any>;
    public loading:boolean;
    constructor(private authenticationService:AuthenticationService, private router: Router) { 
    
        this.errors = [];
 
    }

    ngOnInit() {
    }
    
    public login(form){
        this.loading = true;
        this.authenticationService.login(form).toPromise().then(() => {
            this.loading = false;
            this.router.navigate(['/admin/questions']);
        }).catch((e) => {
            this.loading = false;
            
            if (e && e.error && e.error.error && e.error.error.errors){
            for (var index in e.error.error.errors){
                this.errors = this.errors.concat(e.error.error.errors[index]);
            }  
            }
            else{
                let message = "Incorrect email or password";
                this.errors = [message];
            }  
        });
        
    }

}

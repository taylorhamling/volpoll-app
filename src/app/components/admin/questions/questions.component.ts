import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {QuestionService} from '../../../services/question/question.service';
import {RoleService} from '../../../services/role/role.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {AddQuestionComponent} from './add-question/add-question.component';
import {EditQuestionComponent} from './edit-question/edit-question.component';
import {ConfirmationComponent} from '../../confirmation/confirmation.component';

@Component({
    selector: 'app-questions',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuestionsComponent implements OnInit {
    
    public questions:any;
    public roles:Array<any>;
    public loading:boolean;
    
    constructor(private questionService:QuestionService, private roleService:RoleService, public dialog: MatDialog, public snackBar: MatSnackBar) { 
        
        this.roles = [];
        
        this.roleService.getRoles().toPromise().then((data) => {

            this.roles = data;
        })
        
        this.questions = {1:[], 2: []};
        
        
        this.loading = true;
        
        this.getQuestions();
        

        
    }

    ngOnInit() {
    }
    
    public getQuestions(){
        this.questionService.getQuestions().toPromise().then((data) => {

            this.loading = false;
            this.questions = data;
        }).catch(() => {
            this.loading = false;
        })        
    }
    
    public dropQuestion(event: CdkDragDrop<string[]>, role:string) {

        moveItemInArray(this.questions[role], event.previousIndex, event.currentIndex);

        let order = 1;
        let questions = [];
        for (let question of this.questions[role]){
            question.field_order = order;
            questions.push({id:question.id, field_order:order});
            order +=1;
            
        }
        
        this.questionService.reorderQuestions(questions).toPromise().then(()=>{});        
        
        
    }      
    
    public addQuestion(roleId){
        let dialogRef = this.dialog.open(AddQuestionComponent, {
            width: '400px',
            data: {role_id:roleId}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data && data.field){
                let question = data.field;
                let fieldOrder = this.questions[roleId].length < 1 ? 1 : (this.questions[roleId][this.questions[roleId].length - 1].field_order + 1)
                question.field_order = fieldOrder;
                this.questions[roleId].push(question);
                this.questionService.createQuestion(data.field).toPromise().then((data) => {

                    question.id = data.id;
                    if (question.field_group_id === "create"){
                        question.field_group_id = null;
                        question.field_group = {name:question.new_group};
                        //this.getQuestions();
                    }
                    this.snackBar.open('Question added!', '', {
                       duration: 2000
                    });                      
                })
            }
        })     
    }  
    
    public editQuestion(question, roleId, index){
        let dialogRef = this.dialog.open(EditQuestionComponent, {
            width: '400px',
            data: {question:question}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data && data.field){
                let question = data.field;
                Object.assign(this.questions[roleId][index], question);
                this.questionService.updateQuestion(question).toPromise().then((data) => {
                    
                    if (question.field_group_id === "create"){
                        question.field_group_id = null;
                        question.field_group = {name:question.new_group};
                        //this.getQuestions();
                    }                    
                    
                    this.snackBar.open('Question updated!', '', {
                       duration: 2000
                    });                      
                })
            }
        })         
    }
    
    
    public deleteQuestion(question, roleId, index){
        let dialogRef = this.dialog.open(ConfirmationComponent, {
            width: '300px',
            data: {
                title:"Remove Question?", 
                content:"Are you sure you want to remove this question?"
            }
        });
        
        dialogRef.afterClosed().subscribe(result => {
        
            if (result && result.confirm){
                
                this.questions[roleId].splice(index,1);
                
                this.questionService.deleteQuestion(question.id).toPromise().then((data) => {
                    this.snackBar.open('Question removed!', '', {
                       duration: 2000
                    });                      
                })
                
            }
   
        });          
    }
    
}

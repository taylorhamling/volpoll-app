import { Component, OnInit, Inject, ViewChild, ElementRef, ViewEncapsulation  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar} from '@angular/material';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {QuestionService} from '../../../../services/question/question.service';

@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditQuestionComponent{

    public field:any;
    public fieldGroups:Array<any>;
    public fieldTypes:Array<string>;
    public options:Array<string>;
    
    constructor(public dialogRef: MatDialogRef<EditQuestionComponent>,@Inject(MAT_DIALOG_DATA) public data: any, 
        private dialog: MatDialog, private snackBar:MatSnackBar, private questionService:QuestionService){

        this.field = {type:"Select"};
        
        if (this.data.question){
            Object.assign(this.field, this.data.question);
            
            this.options = JSON.parse(this.field.options);
        }        
        
        
        this.fieldGroups = [];
        
        this.questionService.getQuestionGroups().toPromise().then((data) => {
            this.fieldGroups = data;
        });
        
        this.fieldTypes = ["Select", "Free Text", "Select + Free Text", "Intro"]
        
        
        this.options = [
            "Strongly Disagree",
            "Disagree",
            "In Between",
            "Agree",
            "Strongly Agree"
        ];

                
    }
 
    
    public save(){
        this.field.options = JSON.stringify(this.options);
        
        if (this.field.field_group_id && this.field.field_group_id !== "create"){
            for (let group of this.fieldGroups){
                if (group.id === this.field.field_group_id){
                    this.field.field_group = group;
                }
            }
        }
        
        this.dialogRef.close({field:this.field});
    }    
    
    public dismiss(){ 
        this.dialogRef.close();
    }            
         
    ngOnInit(){}    
        

}

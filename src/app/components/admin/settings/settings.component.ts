import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../services/admin/admin.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
    
    
    public settings:any;
    public loading:boolean;
    
    constructor(private adminService:AdminService) { 
    
        this.settings = {
            website_name:"VolPoll",
            website_status:"online",
            start_fields: ["first_name", "last_name", "email", "group_type", "group", "post_code", "position", "is_peak"]
        }
        
        this.loading = true;
        this.adminService.getSettings().toPromise().then((data) => {

            
            for (let setting of data){
                if (setting.key === "website_name"){
                    this.settings.website_name = setting.data;
                }
                else if (setting.key === "website_status"){
                    this.settings.website_status = setting.data;
                }
                else if (setting.key === "start_fields"){
                    this.settings.start_fields = JSON.parse(setting.data);
                }                
                
                
                
            }
            
            
            this.loading = false;
            
        })
        
    
    }

    ngOnInit() {
    }
    
    
    public updateSetting(settingKey){

        
        let settingData = this.settings[settingKey];
        if (settingKey === "start_fields"){
            settingData = JSON.stringify(settingData);
        }
        
        this.adminService.updateSetting({key: settingKey, data: settingData}).toPromise().then(() => {
            
        })
    }
    
}

import { Component, OnInit, Inject, ViewChild, ElementRef, ViewEncapsulation  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubmissionComponent implements OnInit {
    
    
    public submission:any;
    
    constructor(public dialogRef: MatDialogRef<SubmissionComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { 
    
        this.submission = data.submission;

    
    }

    ngOnInit() {
    }
    
    public dismiss(){ 
        this.dialogRef.close();
    }     
    
    

}

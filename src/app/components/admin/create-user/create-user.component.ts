import { Component, OnInit, Inject, ViewChild, ElementRef, ViewEncapsulation  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateUserComponent{

    public user:any;
    
    constructor(public dialogRef: MatDialogRef<CreateUserComponent>,@Inject(MAT_DIALOG_DATA) public data: any){

        this.user = {};
     
            
    }
 
    
    public save(){

        this.dialogRef.close({user:this.user});
    }    
    
    public dismiss(){ 
        this.dialogRef.close();
    }            
         
    ngOnInit(){}    
        

}

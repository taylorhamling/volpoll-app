import { Component, OnInit } from '@angular/core';
import {ReportService} from '../../../services/report/report.service';
import {MatDialog} from '@angular/material';
import {StatsService} from '../../../services/stats/stats.service';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {environment} from '../../../../environments/environment';
import { AdminReportComponent } from '../admin-report/admin-report.component';

import * as moment from 'moment';

import { Chart } from 'angular-highcharts';

@Component({
    selector: 'app-stats',
    templateUrl: './stats.component.html',
    styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
    
    public stats:any;
    public loading:boolean;
    public reportLoading:boolean;
    
    public submissionsPieChart:any;
    public rolesPieChart:any;
    public submissionsChart:any;
    public polarChart:any;
    public averagesChart:any;
    public overviewChart:any;
    
    public report:any;
    
    constructor(private statsService: StatsService, private reportService:ReportService, private authenticationService: AuthenticationService, public dialog: MatDialog) { 
        
        this.loading = true;
        this.reportLoading = true;
        
        this.report = {};
        

        this.authenticationService.getAdminAccessToken().toPromise().then((data) => {

            this.report = {};
            this.report.download = environment.apiUrl + "/adminreport?token=" + data;
            this.reportLoading = false;                       
        })        
        
        
        /*
        this.reportService.getAdminReport().toPromise().then((data) => {

            this.report = data;
            this.report.download = environment.reportUrl + "/" + this.report.file;
            this.reportLoading = false;
        }).catch(() => {
            this.authenticationService.getAdminAccessToken().toPromise().then((data) => {
        
                this.report = {};
                this.report.download = environment.apiUrl + "/adminreport?token=" + data;
                this.reportLoading = false;                       
            })
     
        }) 
        
        */       
        
        this.stats = {};
      
        this.submissionsPieChart = new Chart(this.statsService.getPieConfig());
        this.rolesPieChart = new Chart(this.statsService.getPieConfig());
        this.submissionsChart = new Chart(this.statsService.getLineConfig());
        this.polarChart = new Chart(this.statsService.getPolarConfig());
        this.averagesChart = new Chart(this.statsService.getBarConfig());
        this.overviewChart = new Chart(this.statsService.getBarConfig());
        
        this.statsService.getStats().toPromise().then((data) => {
            this.stats = data;
            this.loading = false;
            console.log(this.stats);
            this.submissionsChart.removeSeries(0);
            
            this.submissionsChart.addSeries({
                data: this.formatStats(data.submissions_chart),
                name: "Submissions",
                color: '#CC2D43'               
            });
            
            this.submissionsChart.addSeries({
                data: this.formatStats(data.submissions_accumulated_chart),
                name: "Submissions Accumulated",
                color: '#2E476B'               
            });            
            
            
            this.submissionsPieChart.removeSeries(0);
            this.submissionsPieChart.addSeries({
                name: 'Amount',
                colorByPoint: true,
                borderWidth: 0,
                data: [{name:"Completed", y:this.stats.completed, color:"#717171"}, {name: "Not Completed", y:this.stats.not_completed, color:"#b9b9b9"}]      
            });      
            
            this.rolesPieChart.removeSeries(0);
            this.rolesPieChart.addSeries({
                name: 'Amount',
                colorByPoint: true,
                borderWidth: 0,
                data: [{name:"Leaders", y:this.stats.leader_submissions}, {name: "Volunteers", y:this.stats.volunteer_submissions}]      
            });              
                              
 
            this.polarChart.addSeries({
                name: 'Leaders',
                type: 'area', 
                color:"#2e79a7",
                data: [{name:"Sustainability", y:this.stats.polar_chart.leaders.sustainability}, {name: "Safety", y:this.stats.polar_chart.leaders.safety}, {name: "Satisfaction", y:this.stats.polar_chart.leaders.satisfaction}]      
            });       
            
            this.polarChart.addSeries({
                name: 'Volunteers',
                type: 'area', 
                color:"#65a6ce",
                data: [{name:"Sustainability", y:this.stats.polar_chart.volunteers.sustainability}, {name: "Safety", y:this.stats.polar_chart.volunteers.safety}, {name: "Satisfaction", y:this.stats.polar_chart.volunteers.satisfaction}]      
            });                          
            
            this.averagesChart.addSeries({
                name: 'Leaders', 
                color:"#2e79a7",
                data: [{name:"Sustainability", y:this.stats.polar_chart.leaders.sustainability}, {name: "Safety", y:this.stats.polar_chart.leaders.safety}, {name: "Satisfaction", y:this.stats.polar_chart.leaders.satisfaction}]      
            });    
            
            this.averagesChart.addSeries({
                name: 'Volunteers', 
                color:"#65a6ce",
                data: [{name:"Sustainability", y:this.stats.polar_chart.volunteers.sustainability}, {name: "Safety", y:this.stats.polar_chart.volunteers.safety}, {name: "Satisfaction", y:this.stats.polar_chart.volunteers.satisfaction}]      
            });                               
        })
    
    }
    
    private formatStats(data){
        let formatted = [];
        for (let stat of data){
            var date = new Date(stat.x);
            formatted.push([date.getTime(), stat.y]);
        }
        return formatted.sort(function(a, b){
            return a[0] - b[0];
        });        
    }    
 
    
    
    public openReport(){
        let dialogRef = this.dialog.open(AdminReportComponent, {
            width: '1000px',
            data: {report: this.report}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data){
                
            }
        })           
    }
          

    ngOnInit() {
    }

}

import { Component, OnInit, Inject, ViewChild, ElementRef, ViewEncapsulation  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {environment} from '../../../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-admin-report',
  templateUrl: './admin-report.component.html',
  styleUrls: ['./admin-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminReportComponent{

    public reportLink:any;
    
    constructor(public dialogRef: MatDialogRef<AdminReportComponent>,@Inject(MAT_DIALOG_DATA) public data: any, private authenticationService: AuthenticationService, private sanitizer:DomSanitizer){

        this.reportLink = "";
     
        this.authenticationService.getAdminAccessToken().toPromise().then((data) => {

            this.reportLink = this.sanitizer.bypassSecurityTrustResourceUrl(environment.apiUrl + "/adminreport?token=" + data);
                    
        })             
    }
    
    
    public cleanURL(url){
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
 
  
    
    public dismiss(){ 
        this.dialogRef.close();
    }            
         
    ngOnInit(){}    
        

}

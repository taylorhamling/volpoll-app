import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../services/admin/admin.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import * as moment from 'moment';
import { ConfirmationComponent } from '../../confirmation/confirmation.component';
import { CreateUserComponent } from '../create-user/create-user.component';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    
    public users:any;
    public search:string;
    public loading:boolean;
    
    constructor(private adminService:AdminService, public dialog: MatDialog, public snackBar: MatSnackBar) { 
        
        this.loading = true;
        this.users = [];
        this.search = "";
        

        this.getUsers();
    
    }

    ngOnInit() {
    }
    
    public getUsers(){
        this.adminService.getUsers(this.search).toPromise().then((data) => {
            this.users = data;
            this.loading = false;
        }).catch(() => {
            this.loading = false;
        })             
    }    
    
    
    public doSearch(){
        this.adminService.getUsers(this.search).toPromise().then((data) => {
            this.users = data;

        });         
    }
    
    public createUser(){
        let dialogRef = this.dialog.open(CreateUserComponent, {
            width: '300px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data){
                
                this.adminService.createUser(data.user).toPromise().then(() => {
                    this.snackBar.open('User created', 'Dismiss', {
                      duration: 2000,
                      verticalPosition: 'bottom',
                      horizontalPosition: 'start'
                    });    
                    this.getUsers();                   
                })
            }
        })         
    }        
    
    
    
    public deleteUser(user){
        let dialogRef = this.dialog.open(ConfirmationComponent, {
            width: '300px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data){
                
                this.adminService.deleteUser(user.id).toPromise().then(() => {
                    this.snackBar.open('User deleted', 'Dismiss', {
                      duration: 2000,
                      verticalPosition: 'bottom',
                      horizontalPosition: 'start'
                    });    
                    this.getUsers();                   
                })
            }
        })         
    }    
    
    
    
    public formatDate(date){
        return moment(date).format("DD/MM/YYYY");
    }
}

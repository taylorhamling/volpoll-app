import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../services/admin/admin.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import * as moment from 'moment';
import {SubmissionComponent} from '../submission/submission.component';
import { ConfirmationComponent } from '../../confirmation/confirmation.component';

@Component({
    selector: 'app-submissions',
    templateUrl: './submissions.component.html',
    styleUrls: ['./submissions.component.scss']
})
export class SubmissionsComponent implements OnInit {
    
    
    public submissions:any;
    public search:string;
    public loading:boolean;
    public loadingMore:boolean;
    public page:number;
    
    constructor(private adminService:AdminService, public dialog: MatDialog, public snackBar: MatSnackBar) { 
        
        this.loading = true;
        this.loadingMore = false;
        this.submissions = {data:[], count:0};
        this.search = "";
        this.page = 1;
        

        this.getSubmissions();
    
    }

    ngOnInit() {
    }
    
    public getSubmissions(){
        this.adminService.getSubmissions(this.search, this.page).toPromise().then((data) => {
            this.submissions = data;

            this.loading = false;
        }).catch(() => {
            this.loading = false;
        })             
    }    
    
    
    public doSearch(){
        this.page = 1;
        this.adminService.getSubmissions(this.search, this.page).toPromise().then((data) => {
            this.submissions = data;

        });         
    }
    
    
    public loadNextPage(){
        
        this.loadingMore = true;
        this.adminService.getSubmissions(this.search, this.page + 1).toPromise().then((data) => {
                this.page +=1;
                this.loadingMore = false;
            this.submissions.data = this.submissions.data.concat(data.data);
        }).catch(() => {
            //this.page -=1;
            this.loadingMore = false;
        });            
    }
    
    
    public exportSubmissions(){
        
        this.adminService.exportSubmissions().toPromise().then((data) => {

            let file = data.file;
            var link = document.createElement("a");
            link.download = file;
            link.href = file;
            link.click();  


            this.snackBar.open('Submissions downloading...', 'Dismiss', {
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'start'
            });             
            
            
        })

                       
    }
    
    public exportAnswers(){
        this.adminService.exportAnswers().toPromise().then((data) => {

            let file = data.file;
            var link = document.createElement("a");
            link.download = file;
            link.href = file;
            link.click();  


            this.snackBar.open('Answers downloading...', 'Dismiss', {
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'start'
            });             
            
            
        })        
    }
    
    public exportAverages(){
        this.adminService.exportAverages().toPromise().then((data) => {

            let file = data.file;
            var link = document.createElement("a");
            link.download = file;
            link.href = file;
            link.click();  


            this.snackBar.open('Averages downloading...', 'Dismiss', {
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'start'
            });             
            
            
        })        
    }
    
    public viewSubmission(submission){
        let dialogRef = this.dialog.open(SubmissionComponent, {
            width: '1000px',
            data: {submission:submission}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data){
                
            }
        })         
    }
    
    public exportSubmission(submission){
        this.adminService.exportSubmission(submission.id).toPromise().then((data) => {

            let file = data.file;
            var link = document.createElement("a");
            link.download = file;
            link.href = file;
            link.click();  


            this.snackBar.open('Submission downloading...', 'Dismiss', {
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'start'
            });             
            
            
        })        
    }
    
    
    public deleteSubmission(submission, index){
        let dialogRef = this.dialog.open(ConfirmationComponent, {
            width: '300px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data){
                
                this.adminService.deleteSubmission(submission.id).toPromise().then(() => {
                    this.snackBar.open('Submission deleted', 'Dismiss', {
                      duration: 2000,
                      verticalPosition: 'bottom',
                      horizontalPosition: 'start'
                    });  
                    this.submissions.data.splice(index, 1);
                    //this.getSubmissions();                     
                })
            }
        })         
    }
    
    
    public formatDate(date){
        return moment(date).format("DD/MM/YYYY");
    }

}

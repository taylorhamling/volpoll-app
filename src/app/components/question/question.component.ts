import { Component, OnInit} from '@angular/core';
import {QuestionService} from '../../services/question/question.service';
import {AnswerService} from '../../services/answer/answer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication/authentication.service';

@Component({
    selector: 'app-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

    public question:any;
    public options:any;
    public answer:any;
    
    public questionCount:number;

    public loading:boolean;
    
    public profile:any;
    
    constructor(private questionService:QuestionService, private answerService:AnswerService, private route:ActivatedRoute, 
        private router: Router, private authenticationService:AuthenticationService) { 
        this.question = {};
        
        this.questionCount = 99;
        this.questionService.retreiveQuestionCount().toPromise().then((data) => {

            this.questionCount = parseInt(data);
        })     
        
        this.answer = {};   
        
        this.profile = {group_type:"group"};
        this.authenticationService.getUserData().toPromise().then((data) => {
            if (data.profile){
                this.profile = data.profile;
            }
        });
  
    }

    ngOnInit() {
        
        this.route.params.subscribe(params =>{
            this.answer = {};
            
            this.options = 
            [
                "Strongly Disagree",
                "Disagree",
                "In Between",
                "Agree",
                "Strongly Agree"
            ];            
            
            
            this.getQuestion(params.id)
        }); 
        
    }
    
    ngOnDestroy(): void {
    }    
    
    
    private getQuestion(id){
        let questionNumber = this.route.snapshot.params['id'];
        this.question.field_order = questionNumber;
        this.loading = true;
        this.questionService.getQuestion(questionNumber).toPromise().then((data) => {

            this.question = data;
            
            //this.question.question = this.question.question.split("group").join(this.profile.group_type);
            //this.question.description = this.question.description.split("group").join(this.profile.group_type);
            
            this.answer.field_id = this.question.id;
            
  
            this.options = JSON.parse(this.question.options);
            
            this.answerService.getAnswer(this.question.id).toPromise().then((data) => {
                Object.assign(this.answer,data);
            });
            
            this.loading = false;
            
        }).catch(() => {
            //check if at the end
            this.questionService.retreiveQuestionCount().toPromise().then((data) => {

                this.questionCount = parseInt(data);
                if (this.question.field_order >= this.questionCount){
                    this.router.navigate(['/end/']); 
                }
            });
            
            this.loading = false;
        })       
    }
    
    
    public selectAnswer(answer){
        
        if (this.answer.answer === (answer + 1)){
            this.answer.answer = null;
        }   
        else{
            this.answer.answer = (answer + 1);
        }
        
        
    }
    
    public next(){

        document.getElementById("question").style.left = "-50px";
        document.getElementById("question").style.opacity = "0";
        
        setTimeout(() => {
            document.getElementById("question").style.left = "50px";           
        },200);
        
        setTimeout(() => {  
            document.getElementById("question").style.left = "0px";            
            document.getElementById("question").style.opacity = "1"; 
        
        
        
        
        
            if (!this.answer.answer){
                this.answer.skipped = true;
            }
            else{
                this.answer.skipped = null;
            }

            this.answerService.saveAnswer(this.answer).toPromise().then(() => {});


            if (this.question.field_order >= this.questionCount){
                //go to end
                this.router.navigate(['/end/']); 
            }
            else{
               this.router.navigate(['/question/' + (parseInt(this.question.field_order) + 1)]); 
            }
        
        },400);
   
    }
    
    public previous(){
        
        document.getElementById("question").style.left = "50px";
        document.getElementById("question").style.opacity = "0";
        
        setTimeout(() => {
            document.getElementById("question").style.left = "-50px";           
        },200);
        
        setTimeout(() => {  
            document.getElementById("question").style.left = "0px";            
            document.getElementById("question").style.opacity = "1";         
        
        
            this.router.navigate(['/question/' + (this.question.field_order - 1)]);
        },400);
    }
    
    
    public filterGroupType(text){

        return text.split("group").join(this.profile.group_type);
    }

}

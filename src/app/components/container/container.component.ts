import { Component, OnInit,ViewEncapsulation,ViewChild,ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import {MatDialog, MatSnackBar} from '@angular/material';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {QuestionService} from '../../services/question/question.service';
import {EditUserDetailsComponent} from './edit-user-details/edit-user-details.component';
import {AdminService} from '../../services/admin/admin.service';

@Component({
    selector: 'app-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
    
    public sidenavOpened:boolean;
    public account:any;
    public loading:boolean;
    public websiteStatus:string;
    
    public questionCount:number;
    public currentQuestion:number;
    
    constructor(private authenticationService:AuthenticationService, private router: Router, 
        private questionService:QuestionService, public dialog: MatDialog, public snackBar: MatSnackBar, private adminService:AdminService,
        private titleService: Title ) { 
    
        this.account = {};
        this.loading = true;
        
        
        this.adminService.getSettings().toPromise().then((data) => {

            
            for (let setting of data){
                if (setting.key === "website_name"){
                    this.titleService.setTitle( setting.data );
                }  
                else if (setting.key === "website_status" && setting.data === "Offline" && this.router.url.indexOf("admin") < 0){
                    this.websiteStatus = setting.data;
                }              
                
                
                
            }
            
        })         
        
        
        this.authenticationService.getLocalUserData().toPromise().then((data) => {
            if (data){
                this.account = JSON.parse(data);
                if (this.account.permission_id === 1){
                    this.loading = false;
                }
            }
        })
        
        
        
        this.questionCount = 0;
        this.questionService.getQuestionCount().subscribe((data:any) => {

            if (data){
                this.questionCount = parseInt(data);
            }
        });
        
        this.currentQuestion = 0;
        this.questionService.getCurrentQuestion().subscribe((data:any) => {

            if (data){
                this.currentQuestion = parseInt(data);
            }
        })
        
        
        this.authenticationService.isAuthorized().toPromise().then((data) => {
            //redirect to questions

            
            
            if (data){                    
                this.getUserData();
            } 
            else{
                this.loading = false;
                if (this.router.url === "/admin" || this.router.url === "/admin/login"){
                }
                else{                    
                    
                    this.router.navigate(['/start']);
                }
            }         
            
            
            
        }).catch(() => {
            this.loading = false;
            if (this.router.url === "/admin" || this.router.url === "/admin/login"){
            }
            else{                    
                
                this.router.navigate(['/start']);
            }
        });
        
        
        this.authenticationService.getAuthStatus().subscribe((isRegistered:boolean) => {
            if (isRegistered){
                this.getUserData();
            }
        })
        

    
    }

    ngOnInit() {
    }
    
    
    private getUserData(){
                this.authenticationService.getUserData().toPromise().then((data) => {


                    this.account = data;
                    let activeQuestionNumber = 1;
                    if (data.active_question){
                        activeQuestionNumber = data.active_question.field_order;
                    }


                    if (this.router.url.indexOf("admin") > -1){
                    }                
                    else if (data.completed){
                        this.router.navigate(['/end/']);
                    }
                    else{
                        //this.questionService.getQuestionsByRole(this.account.profile.role_id).toPromise().then(() => {console.log("here")});
                        this.router.navigate(['/question/' + activeQuestionNumber]);
                    }

                    this.loading = false;
                }).catch(() => {
                    this.router.navigate(['/start']);
                    this.loading = false;                
                });         
    }
    
    
    public editDetails(){
        let dialogRef = this.dialog.open(EditUserDetailsComponent, {
            width: '400px',
            data: {user:this.account.profile}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data && data.user){
                Object.assign(this.account, data.user);
                
                this.authenticationService.updateUserData(data.user).toPromise().then(() => {

                    this.snackBar.open('Details updated!', '', {
                       duration: 2000
                    });  
                    
                    Object.assign(this.account.profile, data.user);
                                        
                })
            }
        })        
    }
    
    public logout(){
        this.authenticationService.logout();
    }

}

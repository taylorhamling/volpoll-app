import { Component, OnInit, Inject, ViewChild, ElementRef, ViewEncapsulation  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';


@Component({
  selector: 'app-edit-user-details',
  templateUrl: './edit-user-details.component.html',
  styleUrls: ['./edit-user-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditUserDetailsComponent{

    public user:any;
    
    constructor(public dialogRef: MatDialogRef<EditUserDetailsComponent>,@Inject(MAT_DIALOG_DATA) public data: any){

        this.user = {};
        
        if (this.data.user){
            Object.assign(this.user, this.data.user);
        }        
            
    }
 
    
    public save(){

        this.dialogRef.close({user:this.user});
    }    
    
    public dismiss(){ 
        this.dialogRef.close();
    }            
         
    ngOnInit(){}    
        

}

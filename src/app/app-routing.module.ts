import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';

import { ContainerComponent } from './components/container/container.component';
import { StartComponent } from './components/start/start.component';
import { QuestionComponent } from './components/question/question.component';
import { EndComponent } from './components/end/end.component';
import { AdminComponent } from './components/admin/admin.component';
import { LoginComponent } from './components/admin/login/login.component';
import { QuestionsComponent } from './components/admin/questions/questions.component';
import { StatsComponent } from './components/admin/stats/stats.component';
import { SubmissionsComponent } from './components/admin/submissions/submissions.component';
import { UsersComponent } from './components/admin/users/users.component';
import { SettingsComponent } from './components/admin/settings/settings.component';

const routes: Routes = [        
  {
    path: '',
    component: ContainerComponent,
    children: 
    [

        {
          path: '',
          redirectTo: 'start',
          pathMatch: 'full'
        },    
        {
          path: 'start',
          component: StartComponent,
        },    
        {
          path: 'question/:id',
          component: QuestionComponent,
        },    
        {
          path: 'end',
          component: EndComponent,
        },    
        {
          path: 'admin',
          component: AdminComponent,
          children: [
            {
              path: 'login',
              component: LoginComponent,
            },
            {
              path: 'questions',
              component: QuestionsComponent,
            },
            {
              path: 'stats',
              component: StatsComponent,
            },
            {
              path: 'submissions',
              component: SubmissionsComponent,
            },
            {
              path: 'users',
              component: UsersComponent,
            },
            {
              path: 'settings',
              component: SettingsComponent,
            }                        
          ]
        }
    ]    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from './services/authentication/authentication.module';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';

import { ContainerComponent } from './components/container/container.component';
import { StartComponent } from './components/start/start.component';
import { QuestionComponent } from './components/question/question.component';
import { EndComponent } from './components/end/end.component';

import { AdminComponent } from './components/admin/admin.component';
import { LoginComponent } from './components/admin/login/login.component';
import { QuestionsComponent } from './components/admin/questions/questions.component';
import { StatsComponent } from './components/admin/stats/stats.component';
import { SubmissionsComponent } from './components/admin/submissions/submissions.component';
import { UsersComponent } from './components/admin/users/users.component';
import { SettingsComponent } from './components/admin/settings/settings.component';


import { AddQuestionComponent } from './components/admin/questions/add-question/add-question.component';
import { EditQuestionComponent } from './components/admin/questions/edit-question/edit-question.component';

import { ShareComponent } from './components/end/share/share.component';
import { SubmissionComponent } from './components/admin/submission/submission.component';

import { EditUserDetailsComponent } from './components/container/edit-user-details/edit-user-details.component';
import { CreateUserComponent } from './components/admin/create-user/create-user.component';
import { AdminReportComponent } from './components/admin/admin-report/admin-report.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,    
    StartComponent,
    QuestionComponent,
    EndComponent,
    AdminComponent,
    LoginComponent,
    QuestionsComponent,
    StatsComponent,
    ConfirmationComponent,
    EditUserDetailsComponent,
    AddQuestionComponent,
    EditQuestionComponent,
    ShareComponent,
    SubmissionsComponent,
    SubmissionComponent,
    UsersComponent,
    SettingsComponent,
    CreateUserComponent,
    AdminReportComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AuthenticationModule,
    FormsModule,
    ReactiveFormsModule, 
    DragDropModule, 
    ShareButtonsModule,
    ChartModule 
  ],
  entryComponents: [AddQuestionComponent, EditQuestionComponent, ConfirmationComponent, EditUserDetailsComponent, ShareComponent, 
  SubmissionComponent, CreateUserComponent, AdminReportComponent],
  providers: [{ provide: HIGHCHARTS_MODULES, useFactory: () => [ more] }],
  bootstrap: [AppComponent]
})
export class AppModule { }

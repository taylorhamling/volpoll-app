import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class StatsService {

    constructor(private http: HttpClient) { }
    
    public getStats(): Observable<any> {
        return this.http.get( environment.apiUrl + '/stats')
        .pipe(tap((response) => {
        }));
    }     
    

    
    public getLineConfig():any{
        return {
            options: {
                chart: {
                    type: 'line',
                    zoomType: 'x',
                    resetZoomButton: {
                        position: {
                            align: 'right', // right by default
                            verticalAlign: 'top',
                            y: 10
                        },
                        relativeTo: 'chart'
                    },                
                    spacingRight: 20,
                    marginLeft:70,
                    marginTop:40
                }
            },
            chart: {
                backgroundColor:"rgba(0,0,0,0)"
            },             
            series: [{
                data: [],
                name: "",
                color: '#CC2D43',
                showInLegend: false
            }],
            title: {
                text: null
            },
            credits: {
              enabled: false
            },            
            yAxis: {title:{text:""},min: 0},
            xAxis: {type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        month: '%e. %b',
                        year: '%b'
                    }},
            tooltip: {
                //headerFormat: '<b>{series.name}</b><br>',
                //pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
            },   
            loading: false
        }
    }    
    
    
    
    public getPieConfig():any{
        return { 
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                backgroundColor:"rgba(0,0,0,0)"
            },

            tooltip: {
                pointFormat: 'People:<b>{point.y:.0f}</b><br>{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    },
                    colors: ["#2e79a7", "#65a6ce"]
                }
            },
            title: {
                text: null
            },
            credits: {
              enabled: false
            },            
            series: [{
                name: 'Amount',
                colorByPoint: true,
                borderWidth: 0,
                data: []
            }]

        }      
        
    }
    
    
    public getPolarConfig():any{
        return {  
            chart: {
                polar: true,
                backgroundColor:"rgba(0,0,0,0)"
            },
            title: {
                text: '',
            },
            pane:[{startAngle:45},{startAngle:90}],

                        xAxis: {
                            categories: ["Safety","Sustainability", "Satisfaction"],
                            tickmarkPlacement: 'on',
                                        tickInterval:1,
                            lineWidth: 0,
                            min: 0,
                            max:3,
                            angle:45

                        },
            yAxis: {
                            gridLineInterpolation: 'polygon',
                            showLastLabel: true,
                            lineWidth: 0,
                            tickInterval:1,
                            min: 1,
                            max:5,
                            angle:-45,
                            verticalAlign: 'bottom',
                            },
            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
            },
            credits: {
                enabled: false
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 70,
                layout: 'vertical',
                borderWidth: 1,
                backgroundColor: "rgba(255,255,255,0.5)"
            },
            series:               
              []
        };         
    }
    
    
    
    public getBarConfig():any{
        return {
                chart: {
                    type: 'bar',
                    backgroundColor:"rgba(0,0,0,0)"
                },
            xAxis: {
                categories: ["Sustainability", "Safety", "Satisfaction"],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                max:5,
                title: {
                    text: 'Average Score',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' points',
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: 0,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: "rgba(255,255,255,0.5)"
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:,.2f}'
                        }
                    }
                }, 
            title: {
                text: null
            },
            series: [],
            func: function(chart) {
                setTimeout(() => {
                    chart.reflow();
                    //The below is an event that will trigger all instances of charts to reflow
                    //$scope.$broadcast('highchartsng.reflow');
                }, 0);
            } 
        };        
    }      
    
    
}

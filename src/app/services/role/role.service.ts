import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class RoleService {

    constructor(private http: HttpClient) { }
    
    
    public getRoles(): Observable<any> {
        return this.http.get( environment.apiUrl + '/roles')
        .pipe(tap((response) => {
        }));
  }    
    
}

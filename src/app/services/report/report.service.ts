import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class ReportService {

    constructor(private http: HttpClient) { }
    
    
    public getReport(): Observable<any> {
        return this.http.get( environment.apiUrl + '/report')
        .pipe(tap((response) => {
        }));
    }    
    
    public getReportHtml(): Observable<any> {
        return this.http.get( environment.apiUrl + '/reporthtml')
        .pipe(tap((response) => {
        }));
    }     
    
    public generateReportHtml(html): Observable<any> {
        return this.http.post( environment.apiUrl + '/reporthtml', {html:html})
        .pipe(tap((response) => {
        }));
    }  
    
          
    public getAdminReport(): Observable<any> {
        return this.http.get( environment.apiUrl + '/adminreport')
        .pipe(tap((response) => {
        }));
    }          
}

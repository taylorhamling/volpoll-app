import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    constructor(private http: HttpClient) { 
        
        
        
    }
    
    
    public getSubmissions(search, page): Observable<any> {

        return this.http.get( environment.apiUrl + '/view?page=' + page + '&search=' + search)
        .pipe(tap((response) => {
        }));
    }  
    
    public exportSubmissions(): Observable<any> {

        return this.http.get( environment.apiUrl + '/export')
        .pipe(tap((response) => {
        }));
    }  
    
    public exportAnswers(): Observable<any> {

        return this.http.get( environment.apiUrl + '/exportanswers')
        .pipe(tap((response) => {
        }));
    }      
    
    public exportAverages(): Observable<any> {

        return this.http.get( environment.apiUrl + '/exportaverages')
        .pipe(tap((response) => {
        }));
    }  
    
    public exportSubmission(id): Observable<any> {

        return this.http.get( environment.apiUrl + '/export/' + id)
        .pipe(tap((response) => {
        }));
    }     
     
    public deleteSubmission(id): Observable<any> {

        return this.http.delete( environment.apiUrl + '/deletesubmission/' + id)
        .pipe(tap((response) => {
        }));
    }         
    
    public getUsers(search): Observable<any> {

        return this.http.get( environment.apiUrl + '/users?search=' + search)
        .pipe(tap((response) => {
        }));
    }     
    
    public getUser(id): Observable<any> {

        return this.http.get( environment.apiUrl + '/users/' + id)
        .pipe(tap((response) => {
        }));
    }  
    
    public createUser(user): Observable<any> {

        return this.http.post( environment.apiUrl + '/users', user)
        .pipe(tap((response) => {
        }));
    }         
    
    public updateUser(user): Observable<any> {

        return this.http.put( environment.apiUrl + '/users/' + user.id, user)
        .pipe(tap((response) => {
        }));
    }     
    
    public deleteUser(id): Observable<any> {

        return this.http.delete( environment.apiUrl + '/users/' + id)
        .pipe(tap((response) => {
        }));
    }       
     
    
    public getSettings(): Observable<any> {

        return this.http.get( environment.apiUrl + '/settings')
        .pipe(tap((response) => {
        }));
    }     
    
    public updateSetting(setting): Observable<any> {

        return this.http.put( environment.apiUrl + '/settings/1', setting)
        .pipe(tap((response) => {
        }));
    }       
}
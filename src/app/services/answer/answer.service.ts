import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class AnswerService {

    constructor(private http: HttpClient) { 
        
        
        
    }
    
    
    public getAnswer(questionId): Observable<any> {
        const questionAnswer: string = <string>localStorage.getItem('volpoll.answer.' + questionId);
        if (questionAnswer){
            let answer = JSON.parse(questionAnswer);
            return of(answer);
        }
        return this.http.get( environment.apiUrl + '/answers/' + questionId)
        .pipe(tap((response) => {
        }));
    }    
    
    
    public saveAnswer(answer): Observable<any> {
        return this.http.post( environment.apiUrl + '/answers', answer)
        .pipe(tap((response) => {
            localStorage.setItem('volpoll.answer.' + answer.field_id, JSON.stringify(response));
            
        }));
    }  
     
    
}

import { Injectable } from '@angular/core';
import { AuthService } from 'ngx-auth';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';

interface AccessData {
  accessToken: string;
  refreshToken: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements AuthService {

private authStatus:BehaviorSubject<boolean>;
    
    
  constructor(private http: HttpClient) {
      
      this.authStatus = new BehaviorSubject<any>(false);
      
      
  }

  public isAuthorized(): Observable<boolean> {
    return this
      .getAccessToken()
      .pipe(map(token => !!token));
  }
  
  public isAdminAuthorized(): Observable<boolean> {
    return this
      .getAdminAccessToken()
      .pipe(map(token => !!token));
  }  

    public getAuthStatus(){
        return this.authStatus.asObservable();
    }  


  public refreshToken(): Observable<any> {
    return this
      .getRefreshToken()
      .pipe(
        switchMap((refreshToken: string) =>
          this.http.post(environment.apiUrl + '/refresh', { refreshToken })
        ),
        tap((response) => {
            //this.saveAccessData(tokens)
        }),
        catchError((err) => {
          this.logout();

          return Observable.throw(err);
        })
      );
  }

  public refreshShouldHappen(response: HttpErrorResponse): boolean {
    return response.status === 401;
  }

  public verifyTokenRequest(url: string): boolean {
    return url.endsWith('/refresh');
  }
  
    public login(user): Observable<any> {
    return this.http.post( environment.apiUrl + '/auth/login', user)
    .pipe(tap((response) => {
        this.saveAdminAccessData({accessToken:response.token, refreshToken:response.token})
    }));
  }
  
    public recoverPassword(user): Observable<any> {
    return this.http.post( environment.apiUrl +'/auth/recovery', user)
    .pipe(tap((response) => {
    }));
  }  
  
    public resetPassword(user): Observable<any> {
    return this.http.post( environment.apiUrl + '/auth/reset', user)
    .pipe(tap((response) => {
    }));
  }
  
  
    public register(user): Observable<any> {
    return this.http.post( environment.apiUrl + '/auth/signup', user)
    .pipe(tap((response) => {
        
        this.saveAccessData({accessToken:response.token, refreshToken:response.token});
        setTimeout(() => {this.authStatus.next(true)},200);
    }));
  }  

  
    public getUserData(){
        return this.http.get( environment.apiUrl + '/users/me')
        .pipe(tap((response:any) => {
            localStorage.setItem('volpoll.userData', JSON.stringify(response));
        }));        
    }
    
  public getLocalUserData(): Observable<string> {
    const userData: string = <string>localStorage.getItem('volpoll.userData');
    return of(userData);
  }    
    
    
    public updateUserData(user){
        return this.http.put( environment.apiUrl + '/users/me', user)
        .pipe(tap((response) => {
        }));        
    }  
    
    
    public sendContact(form): Observable<any> {
        return this.http.post( environment.apiUrl + '/sendcontact', form)
        .pipe(tap((response) => {
        }));    
    }        

  /**
   * Logout
   */
  public logout(): void {
    this.clear();
    location.reload(true);
  }

  /**
   * Save access data in the storage
   *
   * @private
   * @param {AccessData} data
   */
  private saveAccessData({ accessToken, refreshToken }: AccessData) {
    this
      .setAccessToken(accessToken)
      .setRefreshToken(refreshToken);
  }
  
  
    private saveAdminAccessData({ accessToken, refreshToken }: AccessData) {
    this
      .setAdminAccessToken(accessToken)
      .setAdminRefreshToken(refreshToken);
  }
  
  /**
   * Get access token
   * @returns {Observable<string>}
   */
  public getAccessToken(): Observable<string> {
      //TODO check is wrokingh
    const token: string = <string>localStorage.getItem('volpoll.accessToken');
    const adminToken: string = <string>localStorage.getItem('volpoll.admin.accessToken');
    if (adminToken){
        return of(adminToken);
    }
    return of(token);
  }

  /**
   * Get refresh token
   * @returns {Observable<string>}
   */
  public getRefreshToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('volpoll.refreshToken');
    return of(token);
  }
  
  /**
   * Get access token
   * @returns {Observable<string>}
   */
  public getAdminAccessToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('volpoll.admin.accessToken');
    return of(token);
  }

  /**
   * Get refresh token
   * @returns {Observable<string>}
   */
  public getAdminRefreshToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('volpoll.admin.refreshToken');
    return of(token);
  }  

  /**
   * Set access token
   * @returns {TokenStorage}
   */
  public setAccessToken(token: string): AuthenticationService {
    localStorage.setItem('volpoll.accessToken', token);

    return this;
  }

   /**
   * Set refresh token
   * @returns {TokenStorage}
   */
  public setRefreshToken(token: string): AuthenticationService {
    localStorage.setItem('volpoll.refreshToken', token);

    return this;
  }
  
  
  /**
   * Set access token
   * @returns {TokenStorage}
   */
  public setAdminAccessToken(token: string): AuthenticationService {
    localStorage.setItem('volpoll.admin.accessToken', token);

    return this;
  }

   /**
   * Set refresh token
   * @returns {TokenStorage}
   */
  public setAdminRefreshToken(token: string): AuthenticationService {
    localStorage.setItem('volpoll.admin.refreshToken', token);

    return this;
  }  

   /**
   * Remove tokens
   */
  public clear() {
        localStorage.removeItem('volpoll.accessToken');
        localStorage.removeItem('volpoll.refreshToken');
        localStorage.removeItem('volpoll.admin.accessToken');
        localStorage.removeItem('volpoll.admin.refreshToken'); 
        localStorage.removeItem('volpoll.userData');   
        localStorage.removeItem('volpoll.questions');   
        
        localStorage.clear();
        
        /*
        
        var arr = []; // Array to hold the keys
        // Iterate over localStorage and insert the keys that meet the condition into arr
        for (var i = 0; i < localStorage.length; i++){
            if (localStorage.key(i).substring(0,15) == 'volpoll.answer.') {
                arr.push(localStorage.key(i));
            }
        }

        // Iterate over arr and remove the items by key
        for (var i = 0; i < arr.length; i++) {
            localStorage.removeItem(arr[i]);
        }    
        
        */
  }

}

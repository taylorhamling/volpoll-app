import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';



@Injectable({
    providedIn: 'root'
})
export class QuestionService {
    
    
    private currentQuestion:BehaviorSubject<boolean>;
    private questionCount:BehaviorSubject<boolean>;
    
    constructor(private http: HttpClient) { 
    
        this.currentQuestion = new BehaviorSubject<any>("");
        this.questionCount = new BehaviorSubject<any>("");
    
    }
    
    
    public getQuestions(): Observable<any> {
        return this.http.get( environment.apiUrl + '/fields')
        .pipe(tap((response) => {
        }));
    }  
    
    public retreiveQuestionCount(): Observable<any> {
        
        const questionsString: string = <string>localStorage.getItem('volpoll.questions');
        if (questionsString){
            let questions = JSON.parse(questionsString);
            this.setQuestionCount(questions.length);
            return of(questions.length);
        }
        
        
        return this.http.get( environment.apiUrl + '/fieldscount')
        .pipe(tap((response) => {
            this.setQuestionCount(response);
        }));  
    } 
    
    
    public getQuestionCount(){
        return this.questionCount.asObservable();
    }
    
    public setQuestionCount(questionCount){
        this.questionCount.next(questionCount);
    }      
    
    public getCurrentQuestion(){
        return this.currentQuestion.asObservable();
    }
    
    public setCurrentQuestion(QuestionNumber){
        this.currentQuestion.next(QuestionNumber);
    }   
      
    
    public getQuestionsByRole(roleId): Observable<any> {
        return this.http.get( environment.apiUrl + '/fields?role_id=' + roleId)
        .pipe(tap((response) => {
            localStorage.setItem('volpoll.questions', JSON.stringify(response));
        }));
    }       
    
    public getQuestion(questionNumber): Observable<any> {
        this.setCurrentQuestion(questionNumber);
        const questionsString: string = <string>localStorage.getItem('volpoll.questions');
        if (questionsString){
            let questions = JSON.parse(questionsString);
            for (let question of questions){
                if (parseInt(question.field_order) === parseInt(questionNumber)){
                    return of(question);
                }
            }
        }
        
        
        return this.http.get( environment.apiUrl + '/fields/' + questionNumber)
        .pipe(tap((response) => {
        }));
    }      
    
    public createQuestion(question): Observable<any> {
        return this.http.post( environment.apiUrl + '/fields', question)
        .pipe(tap((response) => {
        }));
    }  
    
    public updateQuestion(question): Observable<any> {
        return this.http.put( environment.apiUrl + '/fields/' + question.id, question)
        .pipe(tap((response) => {
        }));
    }      
    
    public deleteQuestion(questionId): Observable<any> {
        return this.http.delete( environment.apiUrl + '/fields/' + questionId)
        .pipe(tap((response) => {
        }));
    }      
    
    
    public reorderQuestions(questions): Observable<any> {
        return this.http.post( environment.apiUrl + '/fields/reorder', {fields:questions})
        .pipe(tap((response) => {
        }));
    }      
    
    
    public getQuestionGroups(): Observable<any> {
        return this.http.get( environment.apiUrl + '/fieldgroups')
        .pipe(tap((response) => {
        }));
    }     
    
}

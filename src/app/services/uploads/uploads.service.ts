import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadsService{
    constructor(private http: HttpClient) { }

    uploadFile(mediaFile: FormData): Observable<any> {
        return this.http.post( environment.apiUrl + '/uploads', mediaFile)
        .pipe(tap((response) => {
        }));       


    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule,MatInputModule,MatCardModule,MatProgressSpinnerModule,MatSnackBarModule,MatToolbarModule,
MatSidenavModule,MatIconModule,MatAutocompleteModule,MatListModule,MatDividerModule,MatDialogModule,MatMenuModule, 
MatCheckboxModule, MatSelectModule, MatProgressBarModule,MatRadioModule, MatDatepickerModule, MatNativeDateModule, 
MatTabsModule, MatSliderModule, MatBadgeModule, MatExpansionModule, MatButtonToggleModule, MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [MatButtonModule,MatInputModule,MatCardModule,MatProgressSpinnerModule,MatSnackBarModule,MatToolbarModule,
MatSidenavModule,MatIconModule,MatAutocompleteModule,MatListModule,MatDividerModule,MatDialogModule,MatMenuModule, 
MatCheckboxModule, MatSelectModule, MatProgressBarModule,MatRadioModule, MatDatepickerModule, MatNativeDateModule, 
MatTabsModule, MatSliderModule, MatBadgeModule, MatExpansionModule, MatButtonToggleModule, MatTooltipModule],
  exports: [MatButtonModule,MatInputModule,MatCardModule,MatProgressSpinnerModule,MatSnackBarModule,MatToolbarModule,
MatSidenavModule,MatIconModule,MatAutocompleteModule,MatListModule,MatDividerModule,MatDialogModule,MatMenuModule, 
MatCheckboxModule, MatSelectModule, MatProgressBarModule,MatRadioModule, MatDatepickerModule, MatNativeDateModule, 
MatTabsModule, MatSliderModule, MatBadgeModule, MatExpansionModule, MatButtonToggleModule, MatTooltipModule],
})
export class MaterialModule { }
